<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name></name>
    <message id="startup-ap-name">
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="checkup-la-name_check_app">
        <source>Software check</source>
        <extracomment>Проверка программного обеспечения</extracomment>
        <translation>Проверка программного обеспечения</translation>
    </message>
    <message id="checkup-la-passed">
        <source>Passed</source>
        <extracomment>Пройдена</extracomment>
        <translation>Пройдена</translation>
    </message>
    <message id="checkup-la-name_check_battery">
        <source>Battery check</source>
        <extracomment>Проверка заряда батареии</extracomment>
        <translation>Проверка заряда батареии</translation>
    </message>
    <message id="checkup-la-name_check_battery_less_then">
        <source>Less than</source>
        <extracomment>Осталось менее</extracomment>
        <translation>Осталось менее</translation>
    </message>
    <message id="checkup-la-name_check_sdcard">
        <source>Memory card storage</source>
        <extracomment>Хранилище карты памяти</extracomment>
        <translation>Хранилище карты памяти</translation>
    </message>
    <message id="checkup-la-name_check_sim">
        <source>SIM check</source>
        <extracomment>Проверка SIM</extracomment>
        <translation>Проверка SIM</translation>
    </message>
    <message id="checkup-la-not_installed_check_sim">
        <source>SIM card not installed</source>
        <extracomment>SIM карта не установлена</extracomment>
        <translation>SIM карта не установлена</translation>
    </message>
    <message id="checkup-la-install_sim">
        <source>Install SIM card</source>
        <extracomment>Установите SIM-карту</extracomment>
        <translation>Установите SIM-карту</translation>
    </message>
    <message id="checkup-bu-complete">
        <source>Complete</source>
        <extracomment>Готово</extracomment>
        <translation>Готово</translation>
    </message>
    <message id="checkup-la-name_check_emm">
        <source>EMM client check</source>
        <extracomment>Проверка готовности EMM клиента</extracomment>
        <translation>Проверка готовности EMM клиента</translation>
    </message>
    <message id="checkup-la-name_check_internet">
        <source>Internet check</source>
        <extracomment>Проверка подключения к Интернет</extracomment>
        <translation>Проверка подключения к Интернет</translation>
    </message>
    <message id="checkup-la-name_check_osversion">
        <source>OS version check</source>
        <extracomment>Проверка версии ОС</extracomment>
        <translation>Проверка версии ОС</translation>
    </message>
    <message id="checkup-la-name_check_policy">
        <source>Policy check</source>
        <oldsource>Roles check</oldsource>
        <extracomment>Проверка политик безопсности</extracomment>
        <translation>Проверка ролей пользователя и администратора</translation>
    </message>
    <message id="checkup-la-name_check_roles">
        <source>Roles check</source>
        <extracomment>Проверка ролей пользователя и администратора</extracomment>
        <translation>Проверка ролей пользователя и администратора</translation>
    </message>
    <message id="checkup-la-name_check_readiness">
        <source></source>
        <oldsource>Readiness check</oldsource>
        <translation type="unfinished">Проверка готовности</translation>
    </message>
    <message id="checkup-la-sdcheck_less_then">
        <source>Less than %1 MB left</source>
        <extracomment>&quot;Осталось менее 800 МБ&quot;</extracomment>
        <translation>&quot;Осталось менее %1 МБ&quot;</translation>
    </message>
    <message id="checkup-la-sdcheck_free_mem_out">
        <source>Free memory on SD card runs out</source>
        <extracomment>Свободная память на SD-карте заканчивается</extracomment>
        <translation>Свободная память на SD-карте заканчивается</translation>
    </message>
    <message id="checkup-la-sdchek_less_then">
        <source></source>
        <translation type="unfinished">Проверка готовности</translation>
    </message>
    <message id="checkup-la-sdcheck_contact_admin_for_new_sd">
        <source>Contact your administrator for a new SD memory card.</source>
        <extracomment>Обратитесь к администратору за новой SD картой памяти</extracomment>
        <translation>Обратитесь к администратору за новой SD картой памяти</translation>
    </message>
    <message id="checkup-la-sdcheck_not_enough">
        <source>Not enough space on the map.</source>
        <extracomment>Недостаточно места на карте</extracomment>
        <translation>Недостаточно места на карте</translation>
    </message>
    <message id="checkup-la-sdcheck_not_installed">
        <source>SD card not installed</source>
        <extracomment>SD-карта не установлена</extracomment>
        <translation>SD-карта не установлена</translation>
    </message>
    <message id="checkup-la-name_check_battery_charge">
        <source>Charge the device</source>
        <extracomment>Зарядите устройство</extracomment>
        <translation>Зарядите устройство</translation>
    </message>
    <message id="checkup-la-name_check_battery_fully_charge">
        <source>We recommend that you fully charge the device</source>
        <extracomment>Рекомендуем полностью зарядить устройство</extracomment>
        <translation>Рекомендуем полностью зарядить устройство</translation>
    </message>
    <message id="checkup-la-name_check_osversion_current_version">
        <source>Current OS version</source>
        <oldsource>Wrong OS version</oldsource>
        <extracomment>Текущая версия ОС</extracomment>
        <translation type="unfinished">Текущая версия ОС</translation>
    </message>
    <message id="checkup-la-name_check_osversion_unexpected_version">
        <source>doesn&apos;t match the expected</source>
        <extracomment>не соответствует ожидаемой</extracomment>
        <translation type="unfinished">не соответствует ожидаемой</translation>
    </message>
    <message id="checkup-la-name_check_internetcheck_nointernet">
        <source>No Internet</source>
        <oldsource>No Internet</oldsource>
        <extracomment>Отсутствует подключение к Интернет</extracomment>
        <translation type="unfinished">Отсутствует подключение к Интернет</translation>
    </message>
</context>
</TS>
