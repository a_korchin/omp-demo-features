TEMPLATE = aux

TS_FILE = $$OUT_PWD/omp-checkup.ts
EE_QM = $$OUT_PWD/omp-checkup_eng_en.qm
TRANSLATIONS = \
    omp-checkup-ru.ts

ts.commands += lupdate $$IN_PWD/.. -ts $$TS_FILE
ts.CONFIG += no_check_exist no_link
ts.output = $$TS_FILE
ts.input = .

for(t, TRANSLATIONS) {
    TRANSLATIONS_IN  += $${_PRO_FILE_PWD_}/$$t
}

qm.files = $$replace(TRANSLATIONS_IN, \.ts, .qm)
qm.path = /usr/share/translations
qm.CONFIG += no_check_exist
qm.commands += lupdate -noobsolete -locations none $$IN_PWD/.. -ts $$TS_FILE $$TRANSLATIONS_IN \
    && lrelease -idbased $$TRANSLATIONS_IN

# should add -markuntranslated "-" when proper translations are in place (or for testing)
engineering_english.path = /usr/share/translations
engineering_english.files = $$EE_QM
engineering_english.commands += lrelease -idbased $$TS_FILE -qm $$EE_QM
engineering_english.CONFIG += no_check_exist
engineering_english.depends = ts
engineering_english.input = $$TS_FILE
engineering_english.output = $$EE_QM

QMAKE_EXTRA_TARGETS += qm ts engineering_english
INSTALLS += qm engineering_english

OTHER_FILES += *.ts
