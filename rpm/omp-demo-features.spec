Name:       omp-demo-features

%{!?qtc_qmake:%define qtc_qmake %qmake}
%{!?qtc_qmake5:%define qtc_qmake5 %qmake5}

Summary:    OMP Demo Features
Version:    0.0.1
Release:    1
Group:      System/Applications
License:    Proprietary
URL:        https://git.omprussia.ru/openmobileplatform/omp-demo-features
Source0:    %{name}-%{version}.tar.bz2
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5Qml)
BuildRequires:  pkgconfig(Qt5Quick)
BuildRequires:  qt5-qttools
BuildRequires:  desktop-file-utils
BuildRequires:  pkgconfig(sailfishsilica) >= 0.24.15
BuildRequires:  pkgconfig(sailfishapp) >= 1.0.2

Requires:       sailfishsilica-qt5 >= 0.10.9

%description
Description OMP DemoFeatures

%prep
%setup -q -n %{name}-%{version}

%build

%qtc_qmake5

make %{?jobs:-j%jobs}

%install
rm -rf %{buildroot}

%qmake5_install

%files
%defattr(-,root,root,-)
%{_libdir}/qt5/qml/ru/omprussia/demofeatures
%{_datadir}/jolla-startupwizard/features/
