TARGET = ompdemofeatures
TEMPLATE = lib 
QT += qml quick
CONFIG += plugin

HEADERS = $$files(*.h)
DISTFILES += qml/*.qml

PLUGIN_PATH = $$[QT_INSTALL_QML]/ru/omprussia/demofeatures

components.files += qmldir *.qml *.js *.svg
components.path = $$PLUGIN_PATH

target.path = $$PLUGIN_PATH

sw_features.files += featurepages
sw_features.path = /usr/share/jolla-startupwizard/features/

OTHER_FILES += *.qml *.js

INSTALLS += target components sw_features

