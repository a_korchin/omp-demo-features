import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    signal featureTerminated

    Label {
        id: labelHeader
        text: "First Feature Page"
        anchors {
            top: parent.top
            topMargin: Theme.paddingLarge
            horizontalCenter: parent.horizontalCenter
        }
        font.pixelSize: Theme.fontSizeLarge
    }

    Label {
        id: labelQR
        text: "Scan QR-code"
        anchors {
            bottom: qrrect.top
            bottomMargin: Theme.paddingMedium
            horizontalCenter: parent.horizontalCenter
        }
        font.pixelSize: Theme.fontSizeMedium
    }

    Rectangle {
        id: qrrect
        anchors.centerIn: parent

        width: parent.width * 0.8
        height: width
        color: "white"
        opacity: 0.5
        radius: 8
        Image {
            id: qrcodeImg
            source: "qr-code.svg"
            sourceSize.width: parent.width * 0.9
            sourceSize.height: parent.height * 0.9
            anchors.centerIn: parent
        }
    }

    Button {
        anchors {
            bottom: parent.bottom
            bottomMargin: Theme.paddingLarge
            horizontalCenter: parent.horizontalCenter
        }
        visible: true
        text: "send featureTerminated()"
        onClicked: {
            featureTerminated()
        }
    }
}


