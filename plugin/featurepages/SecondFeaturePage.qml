import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    signal featureTerminated

    Label {
        id: labelHeader
        text: "Second Feature Page"
        anchors {
            top: parent.top
            topMargin: Theme.paddingLarge
            horizontalCenter: parent.horizontalCenter
        }
        font.pixelSize: Theme.fontSizeLarge
    }

    Button {
        anchors {
            bottom: parent.bottom
            bottomMargin: Theme.paddingLarge
            horizontalCenter: parent.horizontalCenter
        }
        visible: true
        text: "send featureTerminated()"
        onClicked: {
            featureTerminated()
        }
    }
}


